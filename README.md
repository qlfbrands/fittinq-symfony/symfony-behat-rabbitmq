# Symfony Behat RabbitMQ Bundle

The Symfony Behat RabbitMQ Bundle simplifies the process of writing Behat tests for RabbitMQ integration in your Symfony application. This bundle provides a set of Behat context steps that streamline the interaction with RabbitMQ queues and exchanges, allowing you to thoroughly test and validate message flows.

## Table of Contents

- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)

## Introduction

Testing RabbitMQ integration in your Symfony application can be a complex and challenging task. This bundle aims to simplify this process by offering a collection of Behat context steps that enable you to interact with RabbitMQ components seamlessly. With this bundle, you can effortlessly create scenarios to test RabbitMQ message flows, ensuring that your application functions as expected.

## Installation

To integrate the "symfony-behat-rabbitmq" bundle into your Symfony project, follow these steps:

1. Install the bundle using Composer:

    ```bash
    composer require fittinq/symfony-behat-rabbitmq
    ```

2. Register the bundle in your Symfony application. If you are using Symfony Flex, the bundle should be automatically registered. If not, make sure to add it to your `config/bundles.php` file.

## Usage

To utilize the "symfony-behat-rabbitMQ" bundle, follow these steps:

### Create Exchanges

Define your exchanges in your Behat feature file to simulate the RabbitMQ message flow:

```gherkin
And there are exchanges
| source | event  | 
| bakery | bread  | 
| bakery | flour  |
```

### Create Queues with Exchanges

Specify the queues associated with the defined exchanges:

```gherkin
And there are queues
| exchange     | queue              |
| bakery.bread | bakery.bread.baked |
```

### Send Messages to Exchanges

Send messages to the specified exchanges to trigger the RabbitMQ flow:

```gherkin
When a message is sent to bakery.bread
"""
{
    "Type": "Wheat bread",
    "ingredients": [
        "Flour",
        "Water",
        "Eggs"
    ]
}
"""
```

### Assert Message Acknowledgment in Queue

Ensure that the message in the queue has been acknowledged:

```gherkin
Then assert that the message in queue bakery.bread.baked is acked
```

### Configuration

To complete the setup, configure your Behat suite to include the `RabbitMQContext`. Here's an example of how to configure your Behat suite in your `behat.yml` file:

```yaml
default:
   suites:
      default:
         paths:
            - features
         contexts:
            - Fittinq\Symfony\Behat\RabbitMQ\Context\RabbitMQContext
```

