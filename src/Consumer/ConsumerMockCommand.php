<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Consumer;

use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Throwable;

class ConsumerMockCommand extends Command
{
    private RabbitMQ $rabbitMQ;
    private CacheInterface $cache;

    public function __construct(RabbitMQ $rabbitMQ, CacheInterface $cache)
    {
        parent::__construct('behat:mock:consume');

        $this->rabbitMQ = $rabbitMQ;
        $this->cache = $cache;
    }

    protected function configure(): void
    {
        parent::configure();

        $this->addArgument('source', InputArgument::REQUIRED);
        $this->addArgument('event', InputArgument::REQUIRED);
        $this->addArgument('target', InputArgument::REQUIRED);
    }

    /**
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $source = $input->getArgument('source');
        $event = $input->getArgument('event');
        $target = $input->getArgument('target');

        $queue = $this->rabbitMQ->getQueue("$source.$event.$target");
        $queue->consume(new ConsumerMockHandler($this->cache, $source, $event, $target));

        return Command::SUCCESS;
    }
}
