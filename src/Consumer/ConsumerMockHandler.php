<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Consumer;

use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use stdClass;
use Symfony\Contracts\Cache\CacheInterface;

class ConsumerMockHandler extends Handler
{
    private CacheInterface $cache;
    private string $source;
    private string $event;
    private string $target;

    public function __construct(CacheInterface $cache,  string $source, string $event, string $target)
    {
        $this->cache = $cache;
        $this->source = $source;
        $this->event = $event;
        $this->target = $target;
    }

    private function getName(): string
    {
        return "$this->source.$this->event.$this->target";
    }

    public function handleMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey): void
    {
        $this->cache->delete($this->getName());
        $this->cache->get($this->getName(), function () use ($headers, $body) {
            return json_encode($body, JSON_PRETTY_PRINT);
        });
    }
}
