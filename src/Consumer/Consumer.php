<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Consumer;

use Fittinq\Symfony\RabbitMQ\RabbitMQ\Queue;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Process\Process;
use Symfony\Contracts\Cache\CacheInterface;

class Consumer
{
    private RabbitMQ $rabbitMQ;
    private CacheInterface $cache;
    private string $source;
    private string $event;
    private string $target;
    private Process $process;
    private Queue $queue;

    public function __construct(RabbitMQ $rabbitMQ, CacheInterface $cache, string $source, string $event, string $target)
    {
        $this->rabbitMQ = $rabbitMQ;
        $this->cache = $cache;

        $this->source = $source;
        $this->event = $event;
        $this->target = $target;

        $this->ensureQueue();
        $this->ensureConsumer();
    }

    private function ensureQueue(): void
    {
        $exchange = $this->rabbitMQ->getExchange($this->getExchangeName());
        $this->queue = $this->rabbitMQ->getQueue($this->getQueueName());
        $this->queue->declare();
        $this->queue->bind($exchange);
    }

    private function ensureConsumer(): void
    {
        $this->process = new Process(["bin/console", "behat:mock:consume",  $this->source, $this->event, $this->target]);
        $this->process->start();
    }

    public function getExchangeName(): string
    {
        return "{$this->source}.{$this->event}";
    }

    public function getQueueName(): string
    {
        return "{$this->source}.{$this->event}.{$this->target}";
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getMessage(): mixed
    {
        return $this->cache->get($this->getQueueName(), function () {
            return null;
        });
    }

    /**
     * @throws InvalidArgumentException
     */
    public function stop(): void
    {
        $this->cache->delete($this->getQueueName());
        $this->queue->delete();
        $this->process->stop();
    }
}
