<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Fittinq\Symfony\Behat\RabbitMQ\Assert\AssertMessageAcked;
use Fittinq\Symfony\Behat\RabbitMQ\Assert\AssertMessageNacked;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Wire\AMQPTable;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RabbitMQContext implements Context
{
    protected RabbitMQ $rabbitMQ;
    private HttpClientInterface $httpClient;
    private array $exchangeNames = [];
    private array $queueNames = [];
    private string $vhost;

    public function __construct(RabbitMQ $rabbitMQ, HttpClientInterface $httpClient, string $vhost)
    {
        $this->rabbitMQ = $rabbitMQ;
        $this->httpClient = $httpClient;
        $this->vhost = urlencode($vhost);
    }

    /**
     * @Given /^there are exchanges$/
     *
     * This method requires there to be an "exchange" user with the "ROLE_API_HIP_AMQ_EXCHANGE" role.
     * @see \Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorUserContext::addUsers
     */
    public function createExchanges(TableNode $exchanges): void
    {
        foreach($exchanges as $exchange){
            $this->createExchange($exchange['source'], $exchange['event']);
            $this->exchangeNames[] = "{$exchange['source']}.{$exchange['event']}";
        }
    }

    public function createExchange($source, $event): void
    {
        $exchange = $this->rabbitMQ->getExchange("{$source}.{$event}");
        $exchange->declare(AMQPExchangeType::FANOUT);
    }

    /**
     * @Given /^there are queues$/
     */
    public function createAndBindQueues(TableNode $queues): void
    {
        foreach($queues as $queue){
            $this->createAndBindQueue($queue['exchange'], $queue['queue']);
            $this->queueNames[] = $queue['queue'];
        }
    }

    public function createAndBindQueue($exchangeName, $queueName): void
    {
        $exchange = $this->rabbitMQ->getExchange($exchangeName);

        $queue = $this->rabbitMQ->getQueue($queueName);
        $queue->declare();

        $queue->bind($exchange);
    }

    /**
     * @When /^an?o?t?h?e?r? message is sent to (.*)$/
     */
    public function sendMessage(string $exchangeName, $body = new PyStringNode(["{}"], 0)): void
    {
        $exchange = $this->rabbitMQ->getExchange($exchangeName);
        $exchange->produce(json_decode($body->getRaw()));
    }

    /**
     * @When /^an?o?t?h?e?r? message is sent to (.*) with id (.*)$/
     */
    public function sendMessageWithId(string $exchangeName, string $messageId, $body = new PyStringNode(["{}"], 0)): void
    {
        $exchange = $this->rabbitMQ->getExchange($exchangeName);
        $exchange->produceWithId(json_decode($body->getRaw()), $messageId);
    }

    /**
     * @Then /^assert that the message in queue (.*) is acked$/
     */
    public function assertSingleMessageAcked(string $queue): void
    {
        $this->assertMultipleMessageAcked(1, $queue);
    }

    /**
     * @Then /^([0-9]+) messages? in queue (.*) (?:is|are) acked$/
     */
    public function assertMultipleMessageAcked(int $numberOfMessages, string $queue): void
    {
        $assert = new AssertMessageAcked($this->httpClient, $queue, $this->vhost, $numberOfMessages);
        $assert->wait();
    }

    /**
     * @Then /^assert that the message in queue (.*) is nacked$/
     */
    public function assertMessageNacked(string $queue): void
    {
        $assert = new AssertMessageNacked($this->httpClient, $queue, $this->vhost);
        $assert->wait();
    }

    /**
     * @AfterScenario
     */
    public function deleteExchangesAndQueues(): void
    {
        foreach ($this->queueNames as $queueName){
            $queue = $this->rabbitMQ->getQueue($queueName);
            $queue->delete();
        }
        foreach ($this->exchangeNames as $exchangeName){
            $exchange = $this->rabbitMQ->getExchange($exchangeName);
            $exchange->delete();
        }
    }
}
