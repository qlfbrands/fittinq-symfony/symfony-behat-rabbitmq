<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Fittinq\Symfony\Behat\RabbitMQ\Assert\AssertConsumerReceived;
use Fittinq\Symfony\Behat\RabbitMQ\Assert\AssertConsumerReceivedNothing;
use Fittinq\Symfony\Behat\RabbitMQ\Consumer\Consumer;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;

class ConsumerContext implements Context
{
    private CacheInterface $cache;
    private RabbitMQ $rabbitMQ;

    /**
     * @var Consumer[]
     */
    private array $consumers = [];

    public function __construct(CacheInterface $cache, RabbitMQ $rabbitMQ)
    {
        $this->cache = $cache;
        $this->rabbitMQ = $rabbitMQ;
    }

    /**
     * @Given /^there are consumers$/
     */
    public function ensureConsumers(TableNode $table): void
    {
        foreach ($table as $row) {
            $this->ensureConsumer($row['source'], $row['event'], $row['target']);
        }
    }

    public function ensureConsumer(string $source, string $event, string $target): void
    {
        $this->consumers["{$source}.{$event}.{$target}"] = new Consumer($this->rabbitMQ, $this->cache, $source, $event, $target);
    }

    /**
     * @Then /^no consumers should have received anything$/
     */
    public function assertConsumersReceivedNothing(): void
    {
        foreach ($this->consumers as $consumer) {
            $assert = new AssertConsumerReceivedNothing($consumer);
            $assert->wait();
        }
    }

    /**
     * @Then /^"(.*)" consumer should not have received anything$/
     */
    public function assertSpecificConsumersReceivedNothing(string $queueName): void
    {
        $assert = new AssertConsumerReceivedNothing($this->consumers[$queueName]);
        $assert->wait();
    }

    /**
     * @Then /^all consumers should have received$/
     */
    public function allConsumersShouldHaveReceived(PyStringNode $message): void
    {
        foreach ($this->consumers as $consumer) {
            $assert = new AssertConsumerReceived($consumer, $message->getRaw());
            $assert->wait();
        }
    }

    /**
     * @Then /^"(.*)" consumer should have received$/
     */
    public function consumerShouldHaveReceived(string $consumerName, PyStringNode $message): void
    {
        $assert = new AssertConsumerReceived($this->consumers[$consumerName], $message->getRaw());
        $assert->wait();
    }

    /**
     * @afterScenario
     * @throws InvalidArgumentException
     */
    public function stopConsuming(): void
    {
        foreach ($this->consumers as $consumer) {
            $consumer->stop();
        }
    }
}
