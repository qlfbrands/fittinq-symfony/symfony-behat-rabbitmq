<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyBehatRabbitMQBundle extends Bundle
{
}