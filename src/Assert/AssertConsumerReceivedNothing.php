<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Assert;

use Fittinq\Symfony\Behat\RabbitMQ\Consumer\Consumer;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;
use Psr\Cache\InvalidArgumentException;

class AssertConsumerReceivedNothing extends Waiter
{

    const MAX_WAITING_TIME = 3;

    private Consumer $consumer;
    private bool $received = false;

    public function __construct(Consumer $consumer)
    {
        $this->consumer = $consumer;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function play(): bool
    {
        if ($this->consumer->getMessage() === null) {
            return $this->received = false;
        }

        return $this->received = true;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function onAfterPlay(): void
    {
        Assert::assertFalse($this->received);
    }
}