<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Assert;

use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AssertMessageNacked extends Waiter
{
    private HttpClientInterface $httpClient;
    private bool $acked = false;
    private string $queue;
    private string $vhost;

    public function __construct(HttpClientInterface $httpClient, string $queue, string $vhost)
    {
        $this->httpClient = $httpClient;
        $this->queue = $queue;
        $this->vhost = $vhost;
    }

    /**
     * @throws InvalidArgumentException
     * @throws TransportExceptionInterface
     */
    protected function play(): bool
    {
        $response = $this->httpClient->request(
            'GET',
            "http://dev.rabbitmq.fittinq.com:15672/api/queues/{$this->vhost}/{$this->queue}?columns=message_stats",
            ['auth_basic' => 'guest:guest']
        );

        $data = json_decode($response->getContent());

        if(isset($data->message_stats->ack) && $data->message_stats->ack === 1){
            return $this->acked = true;
        }

        return false;
    }

    protected function onAfterPlay(): void
    {
        Assert::assertFalse($this->acked);
    }
}