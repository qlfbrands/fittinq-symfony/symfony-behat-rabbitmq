<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\RabbitMQ\Assert;

use Fittinq\Symfony\Behat\RabbitMQ\Consumer\Consumer;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;
use Psr\Cache\InvalidArgumentException;


class AssertConsumerReceived extends Waiter
{
    private Consumer $consumer;
    private string $expectedMessage;

    public function __construct(Consumer $consumer, string $expectedMessage)
    {

        $this->consumer = $consumer;
        $this->expectedMessage = $expectedMessage;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function play(): bool
    {
        if (json_decode($this->consumer->getMessage()) == json_decode($this->expectedMessage)){
            return true;
        }
        return false;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function onAfterPlay(): void
    {
        Assert::assertEquals(json_decode($this->expectedMessage), json_decode($this->consumer->getMessage()));
    }
}